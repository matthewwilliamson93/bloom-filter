#ifndef INCLUDED_BLOOM_FILTER
#define INCLUDED_BLOOM_FILTER

#include <cassert>
#include <cmath>
#include <functional>
#include <vector> // TODO: Replace with bdlc_bitarray.h

inline
size_t newHash(size_t x) {
    x = ((x >> 16) ^ x) * 0x45d9f3b;
    x = ((x >> 16) ^ x) * 0x45d9f3b;
    x = (x >> 16) ^ x;
    return x;
}

template <typename T>
class BloomFilter {
  private:
    static constexpr double s_LN2 = M_LN2;
    static constexpr double s_LNS = s_LN2 * s_LN2;

    std::vector<bool> d_bits;
    size_t d_hashes;

    size_t doubleHash(size_t hash1, size_t hash2, size_t n) const;

  public:
    BloomFilter(double falsePostiveRate, size_t expectedSize);
    void add(const T& t);
    bool possiblyContains(const T& t) const;
    void clear();
    size_t expectedCapacity() const;
};

template <typename T>
size_t BloomFilter<T>::doubleHash(size_t hash1, size_t hash2, size_t n) const
{
    return (hash1 + n * hash2) % d_bits.size();
}

template <typename T>
BloomFilter<T>::BloomFilter(double falsePostiveRate, size_t expectedSize) : d_bits(), d_hashes()
{
    assert(0 < falsePostiveRate && falsePostiveRate < 1);
    auto m = static_cast<size_t>((-1 * std::log(falsePostiveRate) * expectedSize) / s_LNS);
    assert(m < d_bits.max_size());

    d_bits.resize(m);
    d_hashes = ceil((m / expectedSize) * s_LN2) + 1;
}

template <typename T>
void BloomFilter<T>::add(const T& t)
{
    // TODO: Replace out these hash functions with real not shitty hash functions
    const size_t hash1 = std::hash<T>{}(t);
    const size_t hash2 = newHash(hash1);
    for (size_t i = 1; i != d_hashes; ++i) {
        d_bits[doubleHash(hash1, hash2, i)] = true;
    }
}

template <typename T>
bool BloomFilter<T>::possiblyContains(const T& t) const
{
    // TODO: Replace out these hash functions with real not shitty hash functions
    const size_t hash1 = std::hash<T>{}(t);
    const size_t hash2 = newHash(hash1);
    for (size_t i = 1; i != d_hashes; ++i) {
        if (!d_bits[doubleHash(hash1, hash2, i)]) {
            return false;
        }
    }
    return true;
}

template <typename T>
size_t BloomFilter<T>::expectedCapacity() const
{
    return d_bits.size();
}

template <typename T>
void BloomFilter<T>::clear()
{
    d_bits.clear();
}

#endif
