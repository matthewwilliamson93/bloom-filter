#include <bloom_filter.h>

#include <gtest/gtest.h>

TEST(bloom_filter, constructor)
{
    BloomFilter<int> bf(0.05, 100);
}

TEST(bloom_filter, add_values)
{
    BloomFilter<int> bf(0.05, 100);
    bf.add(33);
    bf.add(1432);
}

TEST(bloom_filter, lookup_values)
{
    BloomFilter<int> bf(0.05, 100);
    bf.add(33);
    bf.add(1432);
    EXPECT_TRUE(bf.possiblyContains(33));
    EXPECT_TRUE(bf.possiblyContains(1432));
    EXPECT_FALSE(bf.possiblyContains(1732));
}

TEST(bloom_filter, capacity)
{
    BloomFilter<int> bf(0.05, 100);
    EXPECT_EQ(623, bf.expectedCapacity());
}
