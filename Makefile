.PHONY: build
build:
	@echo "Compile / Link and Run Unit Test\n"
	@clang++-6.0 -std=c++14 -Wall -Wextra -Werror -pedantic -I. -I/usr/include bloom_filter.t.cpp -L/usr/lib/ -lgtest_main -l gtest -pthread
	@valgrind ./a.out

.PHONY: clean
clean:
	@echo "Cleaning\n"
	@rm a.out
